# Technical Problem

## Situation

We have a client that needs their submittals data pushed to their Procore instance. An example dataset with a few submittals can be found at [in the data directory of the gitlab repository for this exercise](https://gitlab.com/dpankros/procore-exercise/-/blob/main/data/dataset-1.json).  The raw submittals have extra data that you won’t need, but please review the dataset to understand what data will be provided.  You can assume that the data in the dataset will be typical of production data.

I know you don’t know what the Procore API looks like.  You can access their [reference docs](https://developers.procore.com/reference/rest/v1/authentication) once you create an account (it’s free).  I will also provide you with a `clientId` and `clientSecret` that the client has provided for this purpose.  This authentication information is for a service account on their Procore instance, but they will not be giving us UI access.  Their Procore instance uses the URL `https://sandbox.procore.com`. Please use Procore’s new API endpoint “Rest v1” rather than the deprecated version “Vapid”, as referenced in the docs.  The service account is setup for only the one project where it needs to write so there should only one project that is visible.

The goal is to use Node.js 12, Express 4, and the `node-fetch` library to build a REST webservice to push the data to Procore.  **Do not use Procore javascript libraries or OAuth client libraries for this exercise**. You only need to focus on the create (POST) method for now.  (We don’t have a need to query the information, just to write it.)  Of course, if you need (or want) to write a GET method, for example, that is up to you, but it is not required.  The POST method of your webservice will receive one element of the dataset at a time as JSON and send it to Procore making modifications/adding/removing to the submittal properties, as necessary.  We have another team writing the extract service and they will be sending the data to your webservice in production.  Make the endpoint available at /api/submittals as they have already begun writing the extract portion.

## What is Expected?

Write a webservice with the provided specifications.  To run the webservice in production, our base docker image requires that everything needed to run is provided in npm scripts and with a configurable port.  Namely:
```bash
npm install
PORT=8000 npm start
```
Ensure your code is available on any git hosting site of your choice (github, gitlab, bitbucket, etc) and send a link to the repository to me before the deadline.  If you do not want to keep it public, we can determine a short time window when you can make it public, I can clone it, and then you can switch it back to private.

It is highly recommended to get things to a working state before refining anything.  While some authentication will be required, it is sufficient to simplify things by requesting a new `access_token` before each procore request.

Completion of this problem is expected by 5pm one week from the date this problem is sent.  I expect that you will complete the problem on your own, using whatever online resources you need, but not allowing anyone to write any part of this for you.

Please let me know if you have any questions or need clarification.  If the problem is taking too much of your time, let me know so we can discuss.   Thanks and good luck.

## Notes

This is a simulated exercise.  This exercise is for informational purposes only and will not be billed to or used for a client.
